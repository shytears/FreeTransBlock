# FreeTransBlock（开源积木）

#### 项目介绍
This is an Opensource laser-cutting block patterns projects for mechanics building system, initialize designed by Zhixin makerspace and Digitial Craftman studio.

这是一个开源的、用于搭建机械结构系统的激光切割项目。它包含了一个基本单元的样式及其衍生的各种部件。它是有广州执信中学创客空间与七色堇（CPM）工作室共同设计研发，用于教学以及小规模的创客项目。
该项目目标是设计一套基于最少量部件样式，并适用于教学或者小型创客项目的快速搭建系统。部件最终汇总到若干A4大小的木板，并用激光切割机切制。
项目使用GPL协议，个人及非营利性机构可以免费使用，可以修改后以非盈利方式传播；商业机构可以进行小规模试教试用。如修改项目核心内容（如样式、图形元素布局、基本单元尺寸等）并作盈利性分发或个性化定制，请与作者接洽。
#### ![title](https://images.gitee.com/uploads/images/2018/0707/231242_be4d07b0_2036688.png "title.png")

#### 内容架构说明
- /assem_parts           ———— 该文件夹是用于三维模型装配的基本零件模型文件和装配文件，solidwork格式。
- /assem_pic             ———— 该文件夹是使用积木进行搭建的组件、项目爆炸安装示意图，PNG格式。
- /released_svg          ———— 该文件夹是经过测试的的满幅矢量图形，svg格式。


#### 软件与切割说明



1.  推荐使用inkscape开源矢量图形编辑软件读取SVG图形
1.  推荐使用Solidwork打开零件模型文件或装配文件。
1.  本项目切割文件幅面为A4，所有文件均使用**3毫米A4**幅面的木板进行切割测试并通过。
1.  本项目切割文件经过等比例缩放，可以在30cm×20cm幅面的材料上打印。

#### 贡献内容前必读
如需向本项目贡献内容，建议先使用3D建模软件进行装配以确定方案。然后将装配文件上传到/assem_part文件夹中，并截图上传到/assem_pic文件夹中，最后将使用inkscape将该制作各个零部件的平面图汇总到一个svg图中，幅面为A4。

